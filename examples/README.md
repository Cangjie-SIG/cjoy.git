# 样例

- [simple](simple/src/main.cj)：基本的API使用
- [rest](rest/src/main.cj): Restful风格
- [macros](macros/src/main.cj): 使用宏实现Handler注册与参数展开
- [chuncked](chuncked/src/main.cj): chuncked响应
- [sse](sse/src/main.cj): Server-Sent Event
- [tls](tls/src/main.cj): HTTPS
- [fileserver](fileserver/src/main.cj): 文件服务器
- [websocket](websocket/src/main.cj): WebSocket
- [multipart](multipart/src/main.cj): MultiPart，文件上传